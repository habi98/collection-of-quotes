const path = require('path');

const rootPath = __dirname;


module.exports = {
    rootPath,
    dbUrl: 'mongodb://localhost/collection_quotes',
    mongoOptions: {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        useFindAndModify:  false
    }
};
