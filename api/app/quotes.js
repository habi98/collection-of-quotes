const express = require('express');
const Quote = require('../models/Quote');
const router = express.Router();

router.get('/', (req, res) => {
    const criteria = {};
    if (req.query.category) {
        criteria.category = req.query.category
    }
    Quote.find(criteria)
        .then(result => res.send(result))
        .catch(e => res.send(e))
});

router.post('/', async (req, res) => {
    try {
        const data = req.body;

        const quote = await Quote(data);


        await quote.save();
        return res.send(quote);
    } catch (error) {
        return res.status(400).send(error)
    }
});

router.get('/:id', (req, res) => {
   Quote.findById(req.params.id).populate('category', 'title')
       .then(result => res.send(result))
       .catch(err => res.sendStatus(500))
});

router.put('/:id',async (req, res) => {
    const quoteData = req.body;
    const quote = await Quote.findByIdAndUpdate(req.params.id, quoteData);

    quote.save()
        .then(result => res.send(result))
        .catch(err => res.status(400).send(err))
});

router.delete('/:id', async (req, res) => {
    try {
        const quote = await Quote.findByIdAndDelete(req.params.id);
        await quote.save();
        res.send({message: 'ok'})
    } catch (e) {
        res.status(400).send(e)
    }
});

module.exports = router;