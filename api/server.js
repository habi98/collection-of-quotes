const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');

const quotes = require('./app/quotes');
const categories = require('./app/categories');

const app = express();
app.use(cors());
app.use(express.json());

const port = 8000;



mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
    app.use('/quotes', quotes);
    app.use('/categories', categories);
    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });
});

