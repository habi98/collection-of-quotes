const mongoose = require('mongoose');
const config = require('./config');
const Quote = require('./models/Quote');
const Category = require('./models/Category');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const category = await Category.create(
        {
            title: 'star wars'
        },
        {
            title: 'Famous people'
        },
        {
            title: 'Motivational'
        },
        {
            title: 'Saying'
        },
        {
            title: 'Humour'
        }
    );
    await Quote.create(
        {
           author: 'Ки-Ади-Мунди',
           quoteText: 'Час битвы пробил',
           category: category[0]
        },
        {
            author: 'Дарт Мол',
            quoteText: 'Я позабочусь о том, что бы ты почувствовал каждый надрез и страдал также, как и я',
            category: category[0]
        },
        {
            author: 'Альберт Эйнштейн',
            quoteText: 'Теория — это когда все известно, но ничего не работает. ...',
            category: category[1]
        },
        {
            author: 'Пол Дж. Мейер',
            quoteText: 'Продуктивность – не случайность. Это всегда результат стремления к совершенству, разумного планирования и целенаправленных усилий.',
            category: category[2]
        }
    );

    await connection.close();
};

run().catch(error => {
    console.error('Something went wrong', error);
});