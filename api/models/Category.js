const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const SchemaCategory = new Schema({
    title: {
        type: String,
        required: true
    }
});

const Category = mongoose.model('Category', SchemaCategory);

module.exports = Category;