const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const SchemaQuote = new Schema({
    author: {
        type: String,
        required: true
    },
    quoteText: {
        type: String,
        required: true
    },
    category: {
        type: Schema.Types.ObjectId,
        ref: 'Category',
        required: true
    }
});

const Quote = mongoose.model('Quote', SchemaQuote);

module.exports = Quote;