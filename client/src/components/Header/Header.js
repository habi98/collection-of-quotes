import React from 'react';
import {NavLink} from "react-router-dom";
import './Header.css'
const Header = () => {
    return (
        <header>
            <div className="container header-flex">
                <NavLink to="/">Quotes Central</NavLink>
                <nav className="main-nav">
                    <ul>
                        <li><NavLink to="/">Quotes</NavLink></li>
                        <li><NavLink to="/add/quote">Submit new Quote</NavLink></li>
                    </ul>
                </nav>
            </div>
        </header>
    );
};

export default Header;