import React, {Fragment} from 'react';
import error from '../../assets/images/error.png'

const Error = () => {
    return (
        <Fragment>
            <div className="container error">
                <img src={error} alt="error"/>;
            </div>
        </Fragment>

    )
};

export default Error;