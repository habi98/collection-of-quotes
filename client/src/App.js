import React from 'react';
import {BrowserRouter, Route} from 'react-router-dom'
import {NotificationContainer} from 'react-notifications';
import Header from "./components/Header/Header";
import Main from './containers/Main/Main'
import AddNewQuote from "./containers/AddNewQuote/AddNewQuote";
import EditQuote from './containers/EditQuote/EditQuote';




function App() {
  return (
    <BrowserRouter>
        <Header/>
        <NotificationContainer/>
        <Route path="/" exact component={Main}/>
        <Route path="/quotes/:id" exact component={Main}/>
        <Route path="/add/quote" exact component={AddNewQuote}/>
        <Route path="/edit/quote/:id" exact component={EditQuote}/>
    </BrowserRouter>
  );
}

export default App;
