import React, {Component} from 'react';
import '../AddNewQuote/AddNewQuote.css'
import axios from 'axios'
import {NotificationManager} from "react-notifications";

class EditQuote extends Component {
    state = {
        categoryName: '',
        author: '',
        quoteText: '',
        category: ''
    };
    componentDidMount() {
        this.onQuoteLoaded()
    }

    onQuoteLoaded = () => {
        const quoteId = this.props.match.params.id;

        axios.get(`http://localhost:8000/quotes/${quoteId}`).then(quote => (
            this.setState({
                categoryName: quote.data.category.title,
                author: quote.data.author,
                quoteText: quote.data.quoteText,
                category: quote.data.category._id
            })
        ))
    };
    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = (event) => {
        event.preventDefault();
        const quoteData = {};
        Object.keys(this.state).forEach(quote => {
            if (quote !== 'categoryName' ) {
                quoteData[quote] = this.state[quote]
            }
        });
        const quoteId = this.props.match.params.id;
        axios.put(`http://localhost:8000/quotes/${quoteId}`, quoteData).then(() => {
            NotificationManager.success('Успешно изменен');
            this.onQuoteLoaded()
        });
    };


    render() {
        return (
            <div className="container">
                <h1>Edit</h1>
                <form onSubmit={this.submitFormHandler}>
                    <div className="container-select">
                        <label htmlFor="">Category</label>
                        <select onChange={this.inputChangeHandler} name="category" id="">
                                <option value={this.state.category._id}>{this.state.categoryName}</option>
                        </select>
                    </div>
                    <div className="container-input-name">
                        <label htmlFor="">Author</label>
                        <input type="text" value={this.state.author} onChange={this.inputChangeHandler}  name="author"/>
                    </div>
                    <div className="container-text-quote">
                        <label htmlFor="">Quote text</label>
                        <textarea onChange={this.inputChangeHandler} value={this.state.quoteText} className="text-quote" name="quoteText"/>
                    </div>
                    <button type="submit" onClick={this.createQuote} className="btn-save">Save</button>
                </form>
            </div>
        );
    }
}

export default EditQuote;