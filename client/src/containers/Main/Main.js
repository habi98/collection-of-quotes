import React, {Component, Fragment} from 'react';
import './Main.css'
import {NavLink} from 'react-router-dom';
import axios from 'axios'
import Error from "../../components/Error/Error";
import {NotificationManager} from 'react-notifications'

class Main extends Component {
    state = {
        quotes: [],
        categories: [],
        error: false
    };
    loadQuotes = (category) => {
        let url = 'http://localhost:8000/quotes';
        if (category) {
            url += `?category=${category}`
        }
       axios.get(url)
           .then(this.onQuotesLoaded)
           .catch(this.onError)
    };
    loadCategories = () => {
        axios.get('http://localhost:8000/categories').then(response => {
           this.setState({
               categories: response.data
           })
        })
    };
    componentDidMount() {
        let categoryId = this.props.match.params.id;
        this.loadQuotes(categoryId);
        this.loadCategories();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.match.params.id !== prevProps.match.params.id) {
            this.loadQuotes(this.props.match.params.id)
        }
    }
    onQuotesLoaded = (quotes) => {
        this.setState({
            quotes: quotes.data,
            error: false
        })
    };
    onError = (err) => {
        this.setState({
            error: true
        })
    };
    removeQuote =  (quoteId) => {
        return axios.delete(`http://localhost:8000/quotes/${quoteId}`).then( () => {
            this.loadQuotes()
            NotificationManager.success('Success message', 'Успешно удалено');

        })

    };
    renderItems = (arr) => {
        return arr.length > 0 ? arr.map(item => {
            const {author, quoteText, _id} = item;
            return (
                <div className="quote" key={_id}>
                    <h3>{author}</h3>
                    <p>{quoteText}</p>
                    <NavLink className="link-edit" to={`/edit/quote/${_id}`}>Edit</NavLink>
                    <button onClick={() => this.removeQuote(_id)} className="btn-remove">remove</button>
                </div>
            )
        }): <div>В этой категорий нет цитаты</div>
    };
    render() {
        const {error, quotes} = this.state;
        if (error) {
            return <Error/>
        }

        const items = this.renderItems(quotes);
        return (
            <Fragment>
                <div className="main container">
                    <div className="categories">
                        <ul>
                            <li><NavLink to='/'>All</NavLink></li>
                            {this.state.categories && this.state.categories.map(category => (
                                <li key={category._id}><NavLink to={'/quotes/' + category._id}>{category.title}</NavLink></li>
                            ))}
                        </ul>
                    </div>
                    <div className="quotes">
                        {items}
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default Main;