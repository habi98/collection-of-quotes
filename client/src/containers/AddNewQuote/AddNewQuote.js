import React, {Component} from 'react';
import './AddNewQuote.css';
import axios from 'axios';

class AddNewQuote extends Component {
    state = {
        category: '',
        author: '',
        quoteText: '',
        categories: [],
    };


    componentDidMount() {
        axios.get('http://localhost:8000/categories').then(this.onCategoriesLoaded)
    }
    onCategoriesLoaded = (response) => {
       const categories = response.data;
      this.setState({
          categories,
      })
    };

    inputChangeHandler = (event) => {
       this.setState({
           [event.target.name]: event.target.value
       })
    };

    createQuote = (event) => {
        event.preventDefault();
        const data = {};
        Object.keys(this.state).forEach(key => {
            if (typeof this.state[key] === 'string') {
                data[key] = this.state[key]
            }
        });

       axios.post('http://localhost:8000/quotes', data).then(() => {
            this.props.history.push('/')
        })
    };

    render() {
        return (
            <div className="container">
                <h1>Add New Quote</h1>
                <form>
                    <div className="container-select">
                        <label htmlFor="">Category</label>
                        <select onChange={this.inputChangeHandler} name="category" id="">
                            <option  value="">Выбери категорию</option>
                            {this.state.categories && this.state.categories.map(category => (
                                <option key={category._id} value={category._id}>{category.title}</option>
                            ))}
                        </select>
                    </div>
                    <div className="container-input-name">
                        <label htmlFor="">Author</label>
                        <input type="text" onChange={this.inputChangeHandler}  name="author"/>
                    </div>
                    <div className="container-text-quote">
                        <label htmlFor="">Quote text</label>
                        <textarea onChange={this.inputChangeHandler} value={this.state.quoteText} className="text-quote" name="quoteText"/>
                    </div>
                    <button onClick={this.createQuote} className="btn-save">Save</button>
                </form>
            </div>
        );
    }
}

export default AddNewQuote;